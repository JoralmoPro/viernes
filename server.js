var five = require("johnny-five");
var googleTTS = require('google-tts-api');
var board = new five.Board({
  repl: false
});

var express = require('express');
var app = express();
const PORT = process.env.PORT || 5000;
var http = require('http').createServer(app);
var io = require('socket.io')(http);

const ngrok = require('ngrok');

var moment = require("moment");

var Player = require('player');
var play;
var player;

moment.locale("es");

app.use(express.static(__dirname + '/'));
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

function pendienteAlError(playerC) {
  playerC.on('error', function (err) {
    console.log(err);
  });
}

board.on("ready", function () {
  var led = new five.Led(2);
  var escuchando = new five.Led(5);

  io.sockets.on('connection', function (socket) {
    socket.on('prender', function (usuario) {
      player = new Player("./luzEncendida.mp3");
      pendienteAlError(player);
      player.play();
      led.on();
    });
    socket.on('apagar', function (usuario) {
      player = new Player("./luzApagada.mp3");
      pendienteAlError(player);
      player.play();
      led.off();
    });
    socket.on('hora', function (usuario) {
      googleTTS(moment().format('dddd') + moment().format('LL') + ", y son las " + moment().format('LT'), "es-Co", 1)
        .then((url) => {
          var player = new Player(url);
          pendienteAlError(player);
          player.play();
        })
        .catch((err) => {
          console.err(err.stack);
        })
    });
    socket.on('musicaOn', function (usuario) {
      player = new Player("./summit.mp3");
      player.play();
      play = player;
    });
    socket.on('musicaOff', function (usuario) {
      play.stop();
    });
    socket.on('frases', function (frase) {
      console.log(frase);
    });
    socket.on('escuchar', function () {
      escuchando.on();
    });
    socket.on('dejarDeEscuchar', function () {
      escuchando.off();
    });
  });
});

http.listen(PORT);

(async function () {
  const url = await ngrok.connect(PORT);
  console.log(url);
})();

// googleTTS(moment().format('dddd') + moment().format('LL') + ", y son las " + moment().format('LT'), "es-Co", 1)
//   .then(function (url) {
//     console.log(url); // https://translate.google.com/translate_tts?...
//   })
//   .catch(function (err) {
//     console.error(err.stack);
//   });